package com.ride_zoid.ui.announcement;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AnnouncementViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> mText;

    public AnnouncementViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Announcement's Screen");
    }

    public LiveData<String> getText() {
        return mText;
    }


}