package com.ride_zoid.ui.announcement;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ride_zoid.R;
import com.ride_zoid.ui.zoidbank.ZoidbankViewModel;

public class Announcement extends Fragment {

    private AnnouncementViewModel announcementViewModel;

    public static Announcement newInstance() {
        return new Announcement();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        announcementViewModel =  ViewModelProviders.of(this).get(AnnouncementViewModel.class);

        View root = inflater.inflate(R.layout.announcement_fragment, container, false);
        final TextView textView = root.findViewById(R.id.text_announcement);
        announcementViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });


        return  root;



    }


}