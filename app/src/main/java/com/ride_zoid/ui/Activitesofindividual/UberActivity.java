package com.ride_zoid.ui.Activitesofindividual;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ride_zoid.MainActivity;
import com.ride_zoid.R;
import com.ride_zoid.ui.home.HomeFragment;
import com.ride_zoid.ui.home.HomeViewModel;
import com.ride_zoid.ui.slideshow.SlideshowViewModel;

import java.util.Objects;

public class UberActivity extends Fragment {

    private static final String TAG = UberActivity.class.getSimpleName();
    ImageView uberdummy;
    Button uberendtripbtn;
    View root ;




    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (root != null) {
            if ((ViewGroup)root.getParent() != null)
                ((ViewGroup)root.getParent()).removeView(root);
            return root;
        }
          root = inflater.inflate(R.layout.activity_uber, container, false);
        setRetainInstance(true);

        uberdummy = root.findViewById(R.id.uberdummy);
        uberendtripbtn = root.findViewById(R.id.uberendtrip);

        uberendtripbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG,"uber btn click");
                Fragment oldFragment = (Fragment) getActivity().getSupportFragmentManager().findFragmentByTag("home");

                Bundle bundle = new Bundle();
                bundle.putString("money", "100");
                oldFragment.setArguments(bundle);
                

            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                uberdummy.setVisibility(View.INVISIBLE);
                View someView = root.findViewById(R.id.uberscreen);
                someView.setBackgroundColor(getResources().getColor(R.color.white));
                uberdummy.setVisibility(View.VISIBLE);
                uberdummy.setImageResource(R.drawable.uberdriverappsc);
                //uberendtripbtn.setVisibility(View.VISIBLE);

            }
        },1000);



        return root;
    }




/*
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber);
        Objects.requireNonNull(getSupportActionBar()).hide();

        uberdummy = findViewById(R.id.uberdummy);
        uberendtripbtn = findViewById(R.id.uberendtrip);

        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        final HomeFragment myFragment = new HomeFragment();

        Log.i(TAG,"Uber Activity");

        uberendtripbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle bundle = new Bundle();
                bundle.putString("money", "100");
                myFragment.setArguments(bundle);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                uberdummy.setVisibility(View.INVISIBLE);
                View someView = findViewById(R.id.uberscreen);
                someView.setBackgroundColor(getResources().getColor(R.color.white));
                uberdummy.setVisibility(View.VISIBLE);
                uberdummy.setImageResource(R.drawable.uberdriverappsc);
                uberendtripbtn.setVisibility(View.VISIBLE);

            }
        },1000);

    }
*/

}

