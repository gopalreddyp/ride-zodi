package com.ride_zoid.ui.Activitesofindividual;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ride_zoid.R;

public class DidiActivity extends Fragment
{


    private static final String TAG = UberActivity.class.getSimpleName();
    ImageView dididummy;
    Button didiendtripbtn;
    View root;

    public DidiActivity()
    {
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (root != null) {
            if ((ViewGroup)root.getParent() != null)
                ((ViewGroup)root.getParent()).removeView(root);
            return root;
        }

         root = inflater.inflate(R.layout.activity_didi, container, false);
        setRetainInstance(true);

        dididummy = root.findViewById(R.id.dididummy);
        didiendtripbtn = root.findViewById(R.id.didiendtrip);

        didiendtripbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG,"uber btn click");
                Fragment oldFragment = (Fragment) getActivity().getSupportFragmentManager().findFragmentByTag("home");
                Bundle bundle = new Bundle();
                bundle.putString("money", "100");
                oldFragment.setArguments(bundle);


            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                dididummy.setVisibility(View.INVISIBLE);
                View someView = root.findViewById(R.id.didiscreen);
                //someView.setBackgroundColor(getResources().getColor(R.color.white));
                dididummy.setVisibility(View.VISIBLE);
                dididummy.setImageResource(R.drawable.dididriverappsc);
                //didiendtripbtn.setVisibility(View.VISIBLE);

            }
        },1000);



        return root;
    }




}