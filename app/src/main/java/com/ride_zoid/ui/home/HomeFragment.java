package com.ride_zoid.ui.home;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.ride_zoid.R;
import com.ride_zoid.ui.Activitesofindividual.DidiActivity;
import com.ride_zoid.ui.Activitesofindividual.OlaActivity;
import com.ride_zoid.ui.Activitesofindividual.UberActivity;
import com.ride_zoid.ui.LineReader;

import java.util.List;

import me.aflak.bluetooth.Bluetooth;
import me.aflak.bluetooth.interfaces.BluetoothCallback;
import me.aflak.bluetooth.interfaces.DeviceCallback;
import me.aflak.bluetooth.interfaces.DiscoveryCallback;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = HomeFragment.class.getSimpleName();
    ImageView uberLogo, olaLogo, didiLogo;
    //int clickcount = 0;
    //int globalcount = 0;
    private HomeViewModel homeViewModel;
    RelativeLayout redrelativeLayout;
    RelativeLayout bluerelativeLayout;
    TextView price;
    View redview;
    View blueview;
    Animation anim;
    private long lastTouchTime = 0;
    private long currentTouchTime = 0;
    int appactivecount = 0;
    //int doubleclickcount = 0;
    private DrawerLayout drawer;
    ImageView disposeblelogos, disposeblelogosdidi, disposeblelogosola;
    Intent intent;
    String money = "0";
    View root;
   // int uberdoublecount = 0;
   // int dididoublecount = 0;
    //int oladoublecount = 0;
    private boolean mIsUberSelected, mIsOlaSelected, mIsDidiSelected;
    private Bluetooth bluetooth;


    @Override
    public void onResume() {
        super.onResume();



        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        Log.d(TAG, "onResume");
    }


    @Override
    public void onStop() {
        super.onStop();
/*
        bluetooth.onStop();
*/
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

    }



    @Override
    public void onStart() {
        super.onStart();
         }





    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        if (root != null) {
            if ((ViewGroup) root.getParent() != null)
                ((ViewGroup) root.getParent()).removeView(root);
            return root;
        }

        root = inflater.inflate(R.layout.fragment_home, container, false);

        if (getArguments() != null) {
            money = getArguments().getString("money", "0");
        }


        setRetainInstance(true);
        uberLogo = root.findViewById(R.id.uberlogo);
        olaLogo = root.findViewById(R.id.olalogo);
        didiLogo = root.findViewById(R.id.didilogo);
        disposeblelogos = root.findViewById(R.id.disposeblelogos);
        disposeblelogosdidi = root.findViewById(R.id.disposebledidi);
        disposeblelogosola = root.findViewById(R.id.disposebleola);

        RelativeLayout relativeLayout = root.findViewById(R.id.relativelayout1);

        redrelativeLayout = root.findViewById(R.id.redcorner);
        bluerelativeLayout = root.findViewById(R.id.bluecorner);
        redview = root.findViewById(R.id.reddot);
        blueview = root.findViewById(R.id.bluedot);
        price = root.findViewById(R.id.price);
        String symbol = getColoredSpanned("$", "#ffcc00");
        String pricestring = getColoredSpanned(money, "#ffffff");

        price.setText(Html.fromHtml(symbol + " " + pricestring));


        int fragments = getActivity().getFragmentManager().getBackStackEntryCount();
        Log.i(TAG, "Fragments Count is " + fragments);
        bluetooth = new Bluetooth(getContext());
        bluetooth.setBluetoothCallback(bluetoothCallback);

        uberLogo.setOnClickListener(this);
        olaLogo.setOnClickListener(this);
        didiLogo.setOnClickListener(this);

        disposeblelogos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "From Minimise state", Toast.LENGTH_SHORT).show();

                Fragment fragment = new UberActivity();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.nav_host_fragment, fragment, "home");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();


            }
        });


        disposeblelogosdidi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "From Minimise state", Toast.LENGTH_SHORT).show();

                Fragment fragment = new DidiActivity();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.nav_host_fragment, fragment, "home");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();

            }
        });


        disposeblelogosola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "From Minimise state", Toast.LENGTH_SHORT).show();

                Fragment fragment = new OlaActivity();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.nav_host_fragment, fragment, "home");
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });


        bluetooth.setDiscoveryCallback(new DiscoveryCallback() {
            @Override
            public void onDiscoveryStarted() {

            }

            @Override
            public void onDiscoveryFinished() {

            }

            @Override
            public void onDeviceFound(BluetoothDevice device) {
                bluetooth.startScanning();
                bluetooth.pair(device);
                bluetooth.pair(device, "optional pin");
                List<BluetoothDevice> devices = bluetooth.getPairedDevices();

            }

            @Override
            public void onDevicePaired(BluetoothDevice device)
            {


            }

            @Override
            public void onDeviceUnpaired(BluetoothDevice bluetoothDevice) {

            }

            @Override
            public void onError(int i) {

            }
        });


        bluetooth.setDeviceCallback(new DeviceCallback() {
            @Override
            public void onDeviceConnected(BluetoothDevice bluetoothDevice)
            {
                bluetooth.send("hello, uber");
                bluetooth.connectToDevice(bluetoothDevice);

            }

            @Override
            public void onDeviceDisconnected(BluetoothDevice bluetoothDevice, String s) {

            }

            @Override
            public void onMessage(byte[] bytes)
            {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onConnectError(BluetoothDevice bluetoothDevice, String s) {

            }
        });


        return root;
    }
    BluetoothCallback bluetoothCallback = new BluetoothCallback() {
        @Override
        public void onBluetoothTurningOn() {

        }

        @Override
        public void onBluetoothOn()
        {
            bluetooth.startScanning();

        }

        @Override
        public void onBluetoothTurningOff() {

        }

        @Override
        public void onBluetoothOff() {

        }

        @Override
        public void onUserDeniedActivation() {


        }
    };



    //For Color Method

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uberlogo:
                evaluationOfUberFragment();
                break;
            case R.id.didilogo:
                evaluationOfDidiFragment();
                break;
            case R.id.olalogo:
                evaluationOfOlaFragment();
                break;
        }
    }

    private void evaluationOfOlaFragment() {
        lastTouchTime = currentTouchTime;
        currentTouchTime = System.currentTimeMillis();
        if (currentTouchTime - lastTouchTime < 250) {
            Log.d(TAG, "Double click on OLA button");
            olaLogo.setBackgroundResource(R.drawable.full_circle);

            lastTouchTime = 0;
            currentTouchTime = 0;
            mIsOlaSelected = true;

            handleRedAndBlueBlink();
            getOLAFragment();
        }   else {
            Log.d(TAG, "Single click on OLA button");
            handleBackground(olaLogo);
            handleRedAndBlueBlink();
        }
    }

    private void evaluationOfDidiFragment() {
        lastTouchTime = currentTouchTime;
        currentTouchTime = System.currentTimeMillis();
        if (currentTouchTime - lastTouchTime < 250) {
            Log.d(TAG, "Double click on DIDI button");
            didiLogo.setBackgroundResource(R.drawable.full_circle);

            currentTouchTime = 0;
            lastTouchTime = 0;
            mIsDidiSelected = true;

            handleRedAndBlueBlink();
            getDidiFragment();


        }  else {
            Log.d(TAG, "Single click on Didi button");
            handleBackground(didiLogo);
            handleRedAndBlueBlink();
        }
    }


    private void evaluationOfUberFragment() {
        lastTouchTime = currentTouchTime;
        currentTouchTime = System.currentTimeMillis();
        if (currentTouchTime - lastTouchTime < 250) {
            Log.d(TAG, "Double click on Uber button");
            uberLogo.setBackgroundResource(R.drawable.full_circle);

            currentTouchTime = 0;
            lastTouchTime = 0;
            mIsUberSelected = true;

            bluetooth.onStart();
            if(bluetooth.isEnabled()){
                // doStuffWhenBluetoothOn() ...
                handleRedAndBlueBlink();
                getUberFragment();
            } else {
                bluetooth.showEnableDialog(getActivity());
            }


        } else {
            Log.d(TAG, "Single click on Uber button");
            handleBackground(uberLogo);
            handleRedAndBlueBlink();
        }
    }


    private void handleBackground(ImageView ivBackground) {
        if (ivBackground.getBackground().getConstantState() == getResources().getDrawable(R.drawable.full_circle_dup).getConstantState())
            ivBackground.setBackgroundResource(R.drawable.full_circle);
        else
            ivBackground.setBackgroundResource(R.drawable.full_circle_dup);

        switch (ivBackground.getId()) {
            case R.id.uberlogo:
                mIsUberSelected = !mIsUberSelected;
                break;

            case R.id.didilogo:
                mIsDidiSelected = !mIsDidiSelected;
                break;

            case R.id.olalogo:
                mIsOlaSelected = !mIsOlaSelected;
                break;

        }
    }

    private void handleRedAndBlueBlink() {
        if (checkIfAnyButtonIsSelected()) {
            //this is the case when 2 or more are selected
            if (shouldShowRed()) {
                turnoffblueBlink();
                turnOnRed();
            } else {
                turnoffRedBlink();
                turnOnBlueBlink();
            }
        } else {
            turnoffRedBlink();
            turnoffblueBlink();
        }
    }

    private void getOLAFragment() {
        Fragment fragment = new OlaActivity();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, "home");
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    private void getDidiFragment() {
        Fragment fragment = new DidiActivity();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, "home");
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    private void getUberFragment() {
        Fragment fragment = new UberActivity();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, "home");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private boolean shouldShowRed() {
        if (mIsUberSelected && mIsDidiSelected && mIsOlaSelected)
            return true;
        else if (mIsUberSelected && mIsDidiSelected)
            return true;
        else if (mIsUberSelected && mIsOlaSelected)
            return true;
        else if (mIsDidiSelected && mIsOlaSelected)
            return true;

        else return false;
    }

    private boolean checkIfAnyButtonIsSelected() {
        if (!mIsUberSelected && !mIsDidiSelected && !mIsOlaSelected)
            return false;
        return true;
    }

    private void Offonlineblink() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                blueview.setBackgroundResource(R.drawable.circle_blue);
                bluerelativeLayout.setBackgroundResource(R.drawable.right_corner);

            }
        }, 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redview.setBackgroundResource(R.drawable.circle_red);
                redrelativeLayout.setBackgroundResource(R.drawable.left_corner);

            }
        }, 1000);


    }

    private void turnOnRed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redview.setBackgroundResource(R.drawable.circle_red_dup);
                redrelativeLayout.setBackgroundResource(R.drawable.left_corner_dup);

            }
        }, 300);
    }

    private void turnoffblueBlink() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                blueview.setBackgroundResource(R.drawable.circle_blue);
                bluerelativeLayout.setBackgroundResource(R.drawable.right_corner);

            }
        }, 300);

    }


    private void turnoffRedBlink() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redview.setBackgroundResource(R.drawable.circle_red);
                redrelativeLayout.setBackgroundResource(R.drawable.left_corner);

            }
        }, 300);

    }

    private void turnOnBlueBlink() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                blueview.setBackgroundResource(R.drawable.circle_blue_dup);
                bluerelativeLayout.setBackgroundResource(R.drawable.right_corner_dup);

            }
        }, 300);
    }


    public void drawRhombus(Canvas canvas, Paint paint, int x, int y, int width) {
        int halfWidth = width / 2;

        Path path = new Path();
        path.moveTo(x, y + halfWidth); // Top
        path.lineTo(x - halfWidth, y); // Left
        path.lineTo(x, y - halfWidth); // Bottom
        path.lineTo(x + halfWidth, y); // Right
        path.lineTo(x, y + halfWidth); // Back to Top
        path.close();

        canvas.drawPath(path, paint);
    }
}
