package com.ride_zoid.ui.zoidbank;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ride_zoid.R;
import com.ride_zoid.ui.slideshow.SlideshowViewModel;

public class zoidbank extends Fragment {

    private ZoidbankViewModel zoidbankViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        zoidbankViewModel = ViewModelProviders.of(this).get(ZoidbankViewModel.class);
        View root = inflater.inflate(R.layout.zoidbank_fragment, container, false);
        final TextView textView = root.findViewById(R.id.text_bankzoid);
        zoidbankViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

}