package com.ride_zoid.ui.zoidbank;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ZoidbankViewModel extends  ViewModel {

    private MutableLiveData<String> mText;

    public ZoidbankViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Zoid Bank's Screen");
    }

    public LiveData<String> getText() {
        return mText;
    }
}